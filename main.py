""" CLI for computing the average spendings of neighbourhoods """

import argparse
import json
from collections import namedtuple
from pprint import pprint
from statistics import mean
import sys

from shapely.geometry import Point, Polygon

# wrappers for conveniently representing shoppers and neighbourhoods as tuples
Shopper = namedtuple("Shopper", 'spend coordinates')
Neighbourhood = namedtuple("Neighbourhood", 'name bounds')


def read_args():
    """ Parse arguments from user and attempt to read file contents """

    parser = argparse.ArgumentParser(
        description="Compute average transactions for each neighbourhood")
    parser.add_argument(
        'shoppers', type=str,
        help='Path to json file containing list of shoppers\n' +
        'This path should be accessible by the current user'
    )
    parser.add_argument(
        'neighbourhoods', type=str,
        help='Path to json file containing list of neighbourhoods\n' +
        'This path should be accessible by the current user'
    )
    args = parser.parse_args()

    shoppers_json_file_path = args.shoppers
    neighbourhoods_json_file_path = args.neighbourhoods

    try:
        with open(shoppers_json_file_path, 'r') as json_file:
            shoppers = json.load(json_file)
    except IOError:
        sys.exit(f"Unable to read from shoppers json file.\n" +
              "The provided file path is: '{shoppers_json_file_path}'")

    try:
        with open(neighbourhoods_json_file_path, 'r') as json_file:
            neighbourhoods = json.load(json_file)
    except IOError:
        sys.exit(f"Unable to read from neighbourhoods json file.\n" +
              "The provided file path is: '{neighbourhoods_json_file_path}'")

    return shoppers, neighbourhoods


def parse_shoppers(shoppers_json: tuple):
    """ Convert shoppers json input, into a format that is easier to work with.

    For each shopper, its coordinates is converted into a  shapely Point
    """
    return (
        Shopper(i['properties']['spend'], Point(*i['geometry']['coordinates']))
        for i in shoppers_json['features']
    )


def parse_neighbourhoods(neighbourhoods_json: tuple):
    """ Convert neighbourhoods json input into a format that is easier to work with.

    For each neighbourhood, the coordinates are converted into a shapely Polygon
    """
    return (
        Neighbourhood(i['properties']['name'], Polygon(
            i['geometry']['coordinates'][0]))
        for i in neighbourhoods_json['features']
    )


def compute_average_spendings(shoppers: tuple, neighbourhoods: tuple):
    """ Compute the average spendings for each of the neighbourhoods"""
    averages = {}

    for neighbourhood in neighbourhoods:
        spendings_in_neighbourhood = tuple(
            shopper.spend
            for shopper in shoppers
            if shopper.coordinates.within(neighbourhood.bounds)
        )

        # statistics.mean() expects a non-empty iterable as argument
        averages[neighbourhood.name] = 0.0 if not spendings_in_neighbourhood \
            else mean(spendings_in_neighbourhood)

    return averages


if __name__ == "__main__":
    # pylint: disable=invalid-name

    shoppers_input, neighbourhoods_input = read_args()

    shoppers_collection = tuple(parse_shoppers(shoppers_input))
    neighbourhoods_collection = parse_neighbourhoods(neighbourhoods_input)

    average_spendings = compute_average_spendings(
        shoppers_collection, neighbourhoods_collection)

    pprint(average_spendings, indent=4, depth=2, compact=False, width=50)
