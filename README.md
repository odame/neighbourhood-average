### **Problem**
We have recorded a set of shopping transactions at different shops around Toronto. Each transaction is represented by a point and the dollar amount the shopper spent on that transaction in the file shoppers.json

We are only interested in transactions that occurred in the neighbourhoods represented by the boundaries found in neighbourhoods.json

Both of these files contain GeoJSON formatted data.

### **Task**
Write a python script that calculates the average spend of all transactions for each neighbourhood polygon.

The script should take as input two GeoJSON formatted files, one of the neighbourhoods and one of the shopping transactions (example files have been provided), and output the name each neighbourhood with the average spend of all transaction that occurred within the neighbourhood.

### **Hints**
The library Shapely will be very helpful with the reading and handling of GeoJSON formatted data. I recommend you use it.



# Solution

### **Assumptions**
- A shopper's transaction may be related to multiple neighbourhoods

### **Dependencies**
- This project makes use of [shapely](http://shapely.readthedocs.io) package for manipulating points and polygons

### **Setup & Usage**
```sh
>>> git clone https://gitlab.com/odame/neighbourhood-average.git
>>> cd neighbourhood-average
>>> pip install -r requirements.txt
>>> python main /path/to/shoppers/json/file /path/to/neighbourhoods/json/file
```

